﻿//Collections é uma forma de você armazenar uma ou mais informações na memoria no formato de lista.
//Outro recurso interessante das collection são a capacidade de utilizar recursos mais avançados para gerenciar essas listas na memoria.


/*
 * Utilizando a collection do tipo LIST
 */

//Instanciando um lista com alguns valores
List<int> numeros = new List<int>() { 1, 2, 3 };

//Adicionando valores a lista. (Veja que estamos adicionado valores IGUAIS)
numeros.Add(4);
numeros.Add(2); //Este valor ja tinha sido adicionado
numeros.Add(2); //Este valor ja tinha sido adicionado

//Percorrendo a list e imprimido os valores.
//Notem que estamos utilizando um função de OrderBy para ordenar os numeros antes de serem apresentados para o usuario.
foreach (int numero in numeros.OrderBy(x => x))
{
    Console.WriteLine($"Numero: {numero}");
}

//Contole para cria um espaço entre as linhas:
Console.WriteLine();






/*
 * Utilizando a collection do tipo DICTIONARY
 */

//Instanciando um Dictionary com dois tipo, uma chave (KEY) do tipo string e valores (Value) do tipo Int.
//O Dictionary tem esse recurso de armazenar valores e poder acessar por uma Chave.
Dictionary<string, int> idadePessoa = new Dictionary<string, int>();

//Adicionando os dados no Dictionary
idadePessoa.Add("Joao", 20);
idadePessoa.Add("Maria", 30);

//Percorrendo o Dictionary e imprimido os valores.
//Notem que a variavel do foreach é do tipo KeyValuePair<string, int>, ou seja é a variavel que recebe um unico valor do dicionario.
foreach (KeyValuePair<string, int> item in idadePessoa)
{
    //No momento que vai ser impresso os valores, estamos imprimindo a chave e valore
    Console.WriteLine($"Nome: {item.Key} e Idade: {item.Value}");
}


//Esta é outra forma de acessar os dados do Dictionary, utilizando a chave para obter a idade da Pessoa
Console.WriteLine($"Idade: {idadePessoa["Joao"]}");


//Contole para cria um espaço entre as linhas:
Console.WriteLine();





/*
 * Utilizando a collection do tipo HASHSET
 */

//Instanciando um HashSet com alguns valores
//O HashSet é uma coleção que nao permite valores iguais na lista, ou seja caso tentem interir depois o mesmo valor a propria collection se responsabiliza
//em remover o valore e não deixar ter dois item na memoria iguais.
HashSet<int> numeroUnico = new HashSet<int>() { 1, 2, 3 }; //Iniciando os valores utilizan instancia Inline

//Adicionando valores no HashSet
numeroUnico.Add(4);
numeroUnico.Add(2); //Tentando adicionar o mesmo valor que já havia sido adicionado
numeroUnico.Add(3); //Tentando adicionar o mesmo valor que já havia sido adicionado


//Veja que no momento da impressão os valores iguais foram removidos da memoria e é impresso apenas um numero.
foreach (var item in numeroUnico)
{
    Console.WriteLine($"Numero: {item}");
}