using _22_MVC_PrimeiroProjeto.Models;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;

namespace _22_MVC_PrimeiroProjeto.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }


        //Veja como metodo Notas retorna para uma pagina que esta localizada na pasta Views>Home>Notas.cshtml
        public IActionResult Notas() {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
