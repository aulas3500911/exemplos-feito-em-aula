﻿using Microsoft.AspNetCore.Mvc;

namespace _24_MVC_TrabalhandoComRotas.Controllers
{
    public class ClienteController : Controller
    {

        //Adicionando a notation "Route" para criar um apelido para rota

        [Route("Cliente/Principal")]
        public IActionResult Index()
        {
            return View();
        }
    }
}
