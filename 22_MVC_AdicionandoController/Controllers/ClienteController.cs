﻿using Microsoft.AspNetCore.Mvc;

namespace _23_MVC_AdicionandoController.Controllers
{

    //Controller do tipo MVC, que é utilizada para criar paginas ricas e tem a capacidade de retornar Paginas csHtml que ficam localizada na pasta View
    public class ClienteController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult RetornaJson()
        {
            return Json("Retornando um texto em Json");
        }
    }
}
