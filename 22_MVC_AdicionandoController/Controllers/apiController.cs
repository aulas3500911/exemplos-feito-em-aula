﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace _23_MVC_AdicionandoController.Controllers
{


    //Controller do tipo API, que é utilizada para interligar aplicações e retornam dados no formato Json ou xml, sendo Json o principal.

    [Route("api/[controller]")]
    [ApiController]
    public class apiController : ControllerBase
    {
        [HttpGet]
        public IActionResult Index(string id)
        {
            //Veja api retorna objetos que retornam status, no caso o OK retorna o status 200 que é sucesso na requisição
            return Ok(new
            {
                resultado = $"Retornando informação via controller API",
                StatusCode = 200
            });
        }
    }
}
