﻿
//Exceptions - São uma forma de tratamento do código, evitando a parada abrupta quando ocorre um erro em execução.


//Descomente o exemplo abaixo e veja o erro explodir na tela.

//int divisorZero = 0;
//int resultadoDivisao = 10 / divisor;



List<int> numeros = new List<int>() { 1, 2, 3 };

try
{

    //Descomente as linhas abaixo e veja como o erro é tratado caindo no cath
    //int divisor = 0;
    //int resultado = 10 / divisor;

    numeros.Add(4);
    numeros.Add(2);
    numeros.Add(2);

    foreach (int numero in numeros.OrderBy(x => x))
    {
        Console.WriteLine($"Numero: {numero}");
    }
}
catch (DivideByZeroException ex) //Captura os erro.
{
    //Salvar log erro
    //Exibe uma mensagem para o usuario
    Console.WriteLine(ex.Message);

    //Direcionar para outra pagina
    //Ou faz outras atividades para evidar a parada inesperada 
}
finally //Sempre é executado mesmo que o codigo nao apresente erros
{
    //Salvar log de successo


    Console.WriteLine("Executando o finally");
    numeros.Clear();
    GC.SuppressFinalize(numeros);
}


