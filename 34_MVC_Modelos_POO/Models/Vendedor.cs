﻿using _34_MVC_Modelos_POO.Models.Comum;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace _34_MVC_Modelos_POO.Models
{
    [Table("tb_vendedor")]
    public class Vendedor : Pessoa, IModels
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public DateTime DataCriacao { get; set; }
        public DateTime? DataAtualizacao { get; set; }
        public int CodigoVendedor { get; set; }
        public string Seguimento { get; set; }
        public int Numero { get; set; }
        public string Complemento { get; set; }
        public uint Ativo { get; set; }

        public int EnderecoId { get; set; }
        public virtual Endereco Endereco { get; set; }
        public virtual ICollection<Vendas>? Vendas { get; set; }


    }
}
