﻿using _34_MVC_Modelos_POO.Models.Comum;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace _34_MVC_Modelos_POO.Models
{
    [Table("tb_endereco")]
    public class Endereco : IModels
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public DateTime DataCriacao { get; set; }
        public DateTime? DataAtualizacao { get; set; }
        public string Rua { get; set; }
        public string Bairro { get; set; }
        public string Cep { get; set; }
        public uint Ativo { get; set; }

        public virtual Vendedor? Vendedor { get; set; }
        public virtual Cliente? Cliente { get; set; }
        public virtual Fornecedor? Fornecedor { get; set; }

    }
}
