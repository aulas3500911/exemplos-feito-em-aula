﻿using _34_MVC_Modelos_POO.Models.Comum;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace _34_MVC_Modelos_POO.Models
{
    [Table("tb_vendas")]
    public class Vendas: IModels
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public DateTime DataCriacao { get; set; }
        public DateTime? DataAtualizacao { get; set; }
        public uint Ativo { get; set; }

        public int ClienteId { get; set; }
        public virtual Cliente Cliente { get; set; }

        public int VendedorId { get; set; }
        public virtual Vendedor Vendedor { get; set; }

        public int ProdutoId { get; set; }
        public virtual Produtos Produto { get; set; }
    }
}
