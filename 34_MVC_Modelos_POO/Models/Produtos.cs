﻿using _34_MVC_Modelos_POO.Models.Comum;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace _34_MVC_Modelos_POO.Models
{
    [Table("tb_produtos")]
    public class Produtos: IModels
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public DateTime DataCriacao { get; set; }
        public DateTime? DataAtualizacao { get; set; }
        [Required]
        public string Nome { get; set; }
        [Required]
        public decimal Preco { get; set; }
        [Required]
        public string Categoria { get; set; }
        [Required]
        public string Marca { get; set; }
        public uint Ativo { get; set; }

        public int FornecedorId { get; set; }
        public virtual Fornecedor Fornecedor { get; set; }
        public virtual ICollection<Vendas>? Vendas { get; set; }
    }
}
