﻿namespace _34_MVC_Modelos_POO.Models.Comum
{
    public interface IModels
    {
        int Id { get; set; }
        DateTime DataCriacao { get; set; }
        DateTime? DataAtualizacao { get; set; }
        uint Ativo { get; set; }
    }
}
