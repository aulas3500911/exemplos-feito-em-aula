﻿using System.ComponentModel.DataAnnotations;

namespace _34_MVC_Modelos_POO.Models.Comum
{
    public abstract class Pessoa
    {
        [Required(ErrorMessage = "Campo nome é obrigatorio"), MaxLength(150)]
        public string Nome { get; set; }
        [Required(ErrorMessage = "Campo documento é obrigatorio"), MaxLength(30)]
        public string Documento { get; set; }

        [EmailAddress(ErrorMessage = "Campo email é obrigatorio"), MaxLength(100)]
        public string Email { get; set; }
    }
}
