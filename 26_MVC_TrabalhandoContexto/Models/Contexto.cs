﻿using Microsoft.EntityFrameworkCore;

namespace _26_MVC_TrabalhandoContexto.Models
{
    //Criando a classe de contexto, que é responsavel por conectar com banco de dados e gerenciar as transações
    public class Contexto: DbContext
    {
        public Contexto(DbContextOptions<Contexto> options): base(options)
        {
                    
        }


        //Criando os DBSets que são responsáveis por criar e executar as consultas de CRUD com banco, sendo Select, Update, Insert e Delete
        public DbSet<Cliente> Cliente { get; set; }
        public DbSet<Pedido> Pedidos { get; set; }
    }
}
