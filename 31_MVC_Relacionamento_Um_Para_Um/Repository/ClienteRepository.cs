﻿using _31_MVC_Relacionamento_Um_Para_Um.Models;
using Microsoft.EntityFrameworkCore;

namespace _31_MVC_Relacionamento_Um_Para_Um.Repository
{
    public class ClienteRepository : IClienteRepository
    {
        private readonly Contexto _context;

        public ClienteRepository(Contexto context)
        {
            _context = context;
        }

        public List<Cliente> ListarTodos()
        {
            var clientes = _context
                .Cliente
                .ToList()
                .OrderBy(x => x.Idade)
                .ToList();

            return clientes;
        }

        public Cliente ObterUm(int id)
        {
            var cliente = _context
                .Cliente
                .Include(x => x.Pedido)
                .FirstOrDefault(x => x.Id == id);

            return cliente;
        }

        public Cliente SalvarCliente(ClienteModel model)
        {
            if (string.IsNullOrEmpty(model.Nome))
                model.Nome = "<SEM NOME>";

            var cliente = new Cliente
            {
                Nome = model.Nome,
                Idade = model.Idade,
                Pedido = new Pedido
                {
                    Nome = model.PedidoNome,
                    Valor = model.PedidoValor
                }
            };

            _context.Cliente.Add(cliente);
            _context.SaveChanges();

            return cliente;
        }
        public Cliente EditarCliente(ClienteModel model)
        {
            var cliente = _context
                .Cliente
                .Include(x => x.Pedido)
                .FirstOrDefault(x => x.Id == model.Id);

            if (cliente is not null)
            {
                cliente.Nome = model.Nome;
                cliente.Idade = model.Idade;

                if (cliente?.Pedido is null)
                {
                    cliente.Pedido = new Pedido
                    {
                        Nome = model.PedidoNome,
                        Valor = model.PedidoValor
                    };
                }
                else
                {
                    cliente.Pedido.Nome = model.PedidoNome;
                    cliente.Pedido.Valor = model.PedidoValor;
                }

                _context.Cliente.Update(cliente);
                _context.SaveChanges();

                return cliente;
            }

            return new Cliente();
        }
    }
}
