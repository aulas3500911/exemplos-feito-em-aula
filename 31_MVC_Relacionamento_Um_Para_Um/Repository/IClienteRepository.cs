﻿using _31_MVC_Relacionamento_Um_Para_Um.Models;

namespace _31_MVC_Relacionamento_Um_Para_Um.Repository
{
    public interface IClienteRepository
    {
        List<Cliente> ListarTodos();
        Cliente ObterUm(int id);
        Cliente SalvarCliente(ClienteModel model);
        Cliente EditarCliente(ClienteModel model);
    }
}