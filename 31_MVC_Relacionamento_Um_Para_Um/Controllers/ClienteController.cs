﻿using _31_MVC_Relacionamento_Um_Para_Um.Models;
using _31_MVC_Relacionamento_Um_Para_Um.Repository;
using Microsoft.AspNetCore.Mvc;

namespace _31_MVC_Relacionamento_Um_Para_Um.Controllers
{
    public class ClienteController : Controller
    {
        private readonly IClienteRepository _clienteRepository;

        public ClienteController(IClienteRepository clienteRepository)
        {
            _clienteRepository = clienteRepository;
        }

        public IActionResult Index()
        {
            var clientes = _clienteRepository
                .ListarTodos();

            return View(clientes);
        }

        public IActionResult Detail(int id)
        {
            var cliente = _clienteRepository
                .ObterUm(id);

            return View(new ClienteModel
            {
                Nome = cliente.Nome,
                Idade = cliente.Idade,
                PedidoNome = cliente.Pedido is null ? "" : cliente.Pedido.Nome,
                PedidoValor = cliente.Pedido is null ? 0 : cliente.Pedido.Valor,
            });
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(ClienteModel model)
        {
            if (ModelState.IsValid)
            {
                _clienteRepository
                    .SalvarCliente(model);

                return RedirectToAction("Index");
            }

            return View(model);
        }

        public IActionResult Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var cliente = _clienteRepository.ObterUm(id ?? 0);

            return View(new ClienteModel
            {
                Nome = cliente.Nome,
                Idade = cliente.Idade,
                PedidoNome = cliente.Pedido is null ? "" : cliente.Pedido.Nome,
                PedidoValor = cliente.Pedido is null ? 0 : cliente.Pedido.Valor,
            });
        }

        [HttpPost]
        public IActionResult Edit(int id, ClienteModel model)
        {
            if (ModelState.IsValid)
            {
                model.Id = id;

                _clienteRepository.EditarCliente(model);

                return RedirectToAction("Index");
            }

            return View(model);
        }
    }
}
