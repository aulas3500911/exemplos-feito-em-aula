﻿using Microsoft.EntityFrameworkCore;

namespace _31_MVC_Relacionamento_Um_Para_Um.Models
{
    //Criando a classe de contexto, que é responsavel por conectar com banco de dados e gerenciar as transações
    public class Contexto : DbContext
    {
        public Contexto(DbContextOptions<Contexto> options) : base(options)
        {

        }


        //Criando os DBSets que são responsáveis por criar e executar as consultas de CRUD com banco, sendo Select, Update, Insert e Delete
        public DbSet<Cliente> Cliente { get; set; }
        public DbSet<Pedido> Pedido { get; set; }
    }

    //README

    /*
        Para executa a migration deve utilizar o "Package Manager Console", caso ele não esteja disponivel 
        vá no menu "View>Other Windows>Package Manager Console"


        Execute os comandos Add-Migration e Update-Database para criar a migration e depois atualizar o banco de dados.

        Add-Migration initdb  

        Update-Database
      
     */
}
