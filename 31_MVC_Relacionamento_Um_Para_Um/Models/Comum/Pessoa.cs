﻿using System.ComponentModel.DataAnnotations;

namespace _31_MVC_Relacionamento_Um_Para_Um.Models.Comum
{
    public abstract class Pessoa
    {
        [Required, MaxLength(150)]
        public string Nome { get; set; }
    }
}
