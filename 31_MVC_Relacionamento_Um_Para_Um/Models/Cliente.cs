﻿using _31_MVC_Relacionamento_Um_Para_Um.Models.Comum;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace _31_MVC_Relacionamento_Um_Para_Um.Models
{
    [Table("tb_cliente")]
    public class Cliente : Pessoa, IModel
    {
        [Key]
        public int Id { get; set; }
        public int Idade { get; set; }

        public virtual Pedido Pedido { get; set; }
    }
}
