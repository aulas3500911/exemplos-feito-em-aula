﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace _31_MVC_Relacionamento_Um_Para_Um.Models
{
    public class ClienteModel
    {
        public int Id { get; set; }
        [Required]
        public string Nome { get; set; }
        public int Idade { get; set; }
        [DisplayName("Nome do Pedido")]
        public string PedidoNome { get; set; }
        [DisplayName("Valor do Pedido")]
        public decimal PedidoValor { get; set; }
    }
}
