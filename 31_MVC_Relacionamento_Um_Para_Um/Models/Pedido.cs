﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using _31_MVC_Relacionamento_Um_Para_Um.Models.Comum;

namespace _31_MVC_Relacionamento_Um_Para_Um.Models
{
    // Define a classe como uma entidade mapeada para a tabela "tb_pedido"
    [Table("tb_pedido")]
    public class Pedido: IModel
    {
        // Define a propriedade "Id" como a chave primária da tabela
        [Key]
        public int Id { get; set; }
        [Required]
        public string Nome { get; set; }
        public decimal Valor { get; set; }

        public int ClienteId { get; set; }
        public virtual Cliente Cliente { get; set; }
    }
}
