﻿using _30_MVC_CRUD_Repository_Pattern.Models;
using _30_MVC_CRUD_Repository_Pattern.Repository;
using Microsoft.AspNetCore.Mvc;

namespace _30_MVC_CRUD_Repository_Pattern.Controllers
{
    public class ClienteController : Controller
    {
        private readonly IClienteRepository _clienteRepository;

        public ClienteController(IClienteRepository clienteRepository)
        {
            _clienteRepository = clienteRepository;
        }

        public IActionResult Index()
        {
            var clientes = _clienteRepository
                .ListarTodos();

            return View(clientes);
        }

        public IActionResult Detail(int id)
        {
            var cliente = _clienteRepository
                .ObterUm(id);

            return View(cliente);
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(Cliente model)
        {
            if (ModelState.IsValid)
            {
                _clienteRepository
                    .SalvarCliente(model);

                return RedirectToAction("Index");
            }

            return View(model);
        }

        public IActionResult Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var cliente = _clienteRepository.ObterUm(id ?? 0);

            return View(cliente);
        }

        [HttpPost]
        public IActionResult Edit(int id, Cliente model)
        {
            if (ModelState.IsValid)
            {
                model.Id = id;

                _clienteRepository.EditarCliente(id, model);

                return RedirectToAction("Index");
            }

            return View(model);
        }

        public IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var cliente = _clienteRepository.ObterUm(id ?? 0);

            return View(cliente);
        }

        [HttpPost]
        public IActionResult Delete(int id)
        {
            _clienteRepository.ExcluirCliente(id);
            return RedirectToAction("Index");
        }
    }
}
