﻿using _30_MVC_CRUD_Repository_Pattern.Models;

namespace _30_MVC_CRUD_Repository_Pattern.Repository
{
    public interface IClienteRepository
    {
        List<Cliente> ListarTodos();
        Cliente ObterUm(int id);
        Cliente SalvarCliente(Cliente model);
        Cliente EditarCliente(int id, Cliente model);
        Cliente ExcluirCliente(int id);

    }
}