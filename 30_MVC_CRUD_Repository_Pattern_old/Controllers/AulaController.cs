﻿using _30_MVC_CRUD_Repository_Pattern.Models;
using _30_MVC_CRUD_Repository_Pattern.Repository;
using Microsoft.AspNetCore.Mvc;

namespace _30_MVC_CRUD_Repository_Pattern.Controllers
{
    public class AulaController : Controller
    {
        private readonly IClienteRepository _clienteRepository;

        public AulaController(IClienteRepository clienteRepository)
        {
            _clienteRepository = clienteRepository;
        }

        public IActionResult Index()
        {
            //Lista todos os clientes do banco de dados
            var clientes = _clienteRepository
                .ListarTodos();

            return Json(clientes);
        }

        public IActionResult ObterUmCliente(int id)
        {
            //Localiza um cliente cliente na base de dados baseado no id que foi passado na url
            var cliente = _clienteRepository
                .ObterUm(id);

            return Json(cliente);
        }

        public IActionResult SalvarCliente()
        {
            //Cria o modelo de dados para ser inserido na base de dados
            var cliente = new Cliente
            {
                Nome = "Joao",
                Idade = 25
            };

            //Salva um cliente atravez do modelo de dados
            _clienteRepository
                .SalvarCliente(cliente);

            return Json(cliente);
        }

        public IActionResult EditarCliente()
        {
            //Cria o modelo de dados para ser inserido na base de dados
            var cliente = new Cliente
            {
                Nome = "Nome Editado",
                Idade = 10
            };

            //Metodo de editar recebe um parametro de ID e o modelo com as informações que deseja modificar
            _clienteRepository
                .EditarCliente(1, cliente);

            return Json(cliente);
        }
    }
}
