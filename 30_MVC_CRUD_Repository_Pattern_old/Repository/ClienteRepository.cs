﻿using _30_MVC_CRUD_Repository_Pattern.Models;

namespace _30_MVC_CRUD_Repository_Pattern.Repository
{
    public class ClienteRepository : IClienteRepository
    {
        private readonly Contexto _contexto;

        public ClienteRepository(Contexto contexto)
        {
            _contexto = contexto;
        }

        public List<Cliente> ListarTodos()
        {
            var listaCliente = _contexto
                .Cliente
                .ToList()
                .OrderBy(x => x.Idade)
                .ToList();

            return listaCliente;
        }

        public Cliente ObterUm(int id)
        {
            var cliente = _contexto.Cliente.FirstOrDefault(x => x.Id == id);

            return cliente;
        }

        public Cliente SalvarCliente(Cliente model)
        {
            _contexto.Cliente.Add(model);
            _contexto.SaveChanges();

            return model;
        }
        public Cliente EditarCliente(int id, Cliente model)
        {
            var cliente = _contexto
                .Cliente
                .FirstOrDefault(x => x.Id == id);

            if(cliente is not null)
            {
                //Faz o de-para dos valores para atualizar a base de dados

                cliente.Nome = model.Nome;
                cliente.Idade = model.Idade;

                _contexto.Cliente.Update(cliente);
                _contexto.SaveChanges();

                return cliente;
            }

            return model;

        }
    }
}
