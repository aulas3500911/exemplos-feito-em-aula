﻿using _33_MVC_Relacionamento_Muitos_Para_Muitos.Models;

namespace _33_MVC_Relacionamento_Muitos_Para_Muitos.Repository
{
    public interface IClienteRepository
    {
        List<Cliente> ListarTodos();
        Cliente ObterUm(int id);
        Cliente SalvarCliente(Cliente model, int[] listaPedidos);
        Cliente EditarCliente(int id, Cliente model, int[] listaPedidos);
    }
}
