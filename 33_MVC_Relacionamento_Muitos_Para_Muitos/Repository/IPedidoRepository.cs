﻿using _33_MVC_Relacionamento_Muitos_Para_Muitos.Models;

namespace _33_MVC_Relacionamento_Muitos_Para_Muitos.Repository
{
    public interface IPedidoRepository
    {
        List<Pedido> ListarTodos();
        Pedido ObterUm(int id);
        Pedido SalvarPedido(Pedido model, int[] listaClientes);
        Pedido EditarPedido(int id, Pedido model, int[] listaClientes);
    }
}
