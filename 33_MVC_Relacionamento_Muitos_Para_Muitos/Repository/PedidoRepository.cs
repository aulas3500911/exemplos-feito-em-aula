﻿using _33_MVC_Relacionamento_Muitos_Para_Muitos.Models;
using Microsoft.EntityFrameworkCore;

namespace _33_MVC_Relacionamento_Muitos_Para_Muitos.Repository
{
    public class PedidoRepository : IPedidoRepository
    {
        private readonly Contexto _contexto;

        public PedidoRepository(Contexto contexto)
        {
            _contexto = contexto;
        }

        public List<Pedido> ListarTodos()
        {
            var listaPedido = _contexto
                .Pedido
                .Include(x => x.Clientes) //Carregando todos os dados de cliente para exibir na listagem
                .ToList()
                .OrderBy(x => x.Id)
                .ToList();

            return listaPedido;
        }

        public Pedido ObterUm(int id)
        {
            var Pedido = _contexto
                .Pedido
                .Include(x => x.Clientes)
                .FirstOrDefault(x => x.Id == id);

            if (Pedido is null)
                return new Pedido();

            return Pedido;
        }

        public Pedido SalvarPedido(Pedido model, int[] listaClientes)
        {

            if (listaClientes is not null)
            {
                foreach (var item in listaClientes)
                {
                    var cliente = _contexto.Cliente.Find(item);

                    if (cliente is not null)
                    {
                        if (model.Clientes is null)
                            model.Clientes = new List<Cliente> { cliente };
                        else
                            model.Clientes.Add(cliente);
                    }

                }
            }

            _contexto.Pedido.Add(model);
            _contexto.SaveChanges();

            return model;
        }
        public Pedido EditarPedido(int id, Pedido model, int[] listaClientes)
        {
            var Pedido = _contexto
                .Pedido
                .Include(x => x.Clientes)
                .FirstOrDefault(x => x.Id == id);

            if (Pedido is not null)
            {
                //Faz o de-para dos valores para atualizar a base de dados

                Pedido.Nome = model.Nome;
                Pedido.Valor = model.Valor;

                //Adicionando os Clientes
                if (listaClientes is not null)
                {
                    //Limpando todos os registros e salvalndo apenas os novos registros
                    if (Pedido.Clientes is not null) Pedido.Clientes.Clear();

                    foreach (var item in listaClientes)
                    {
                        var Cliente = _contexto.Cliente.Find(item);

                        if (Cliente is not null)
                        {
                            if (Pedido.Clientes is null)
                                Pedido.Clientes = new List<Cliente> { Cliente };
                            else
                                Pedido.Clientes.Add(Cliente);
                        }

                    }
                }

                _contexto.Pedido.Update(Pedido);
                _contexto.SaveChanges();

                return Pedido;
            }

            return model;

        }
    }
}
