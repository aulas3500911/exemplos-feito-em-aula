﻿using _33_MVC_Relacionamento_Muitos_Para_Muitos.Models;
using Microsoft.EntityFrameworkCore;

namespace _33_MVC_Relacionamento_Muitos_Para_Muitos.Repository
{
    public class ClienteRepository : IClienteRepository
    {
        private readonly Contexto _contexto;

        public ClienteRepository(Contexto contexto)
        {
            _contexto = contexto;
        }

        public List<Cliente> ListarTodos()
        {
            var listaCliente = _contexto
                .Cliente
                .ToList()
                .OrderBy(x => x.Idade)
                .ToList();

            return listaCliente;
        }

        public Cliente ObterUm(int id)
        {
            var cliente = _contexto
                .Cliente
                .Include(x => x.Pedidos)
                .FirstOrDefault(x => x.Id == id);

            if (cliente is null)
                return new Cliente();

            return cliente;
        }

        public Cliente SalvarCliente(Cliente model, int[] listaPedidos)
        {

            if (listaPedidos is not null)
            {
                foreach (var item in listaPedidos)
                {
                    var pedido = _contexto.Pedido.Find(item);

                    if (pedido is not null)
                    {
                        if (model.Pedidos is null)
                            model.Pedidos = new List<Pedido> { pedido };
                        else
                            model.Pedidos.Add(pedido);
                    }
                    
                }
            }


            _contexto.Cliente.Add(model);
            _contexto.SaveChanges();

            return model;
        }
        public Cliente EditarCliente(int id, Cliente model, int[] listaPedidos)
        {
            var cliente = _contexto
                .Cliente
                .Include(x => x.Pedidos)
                .FirstOrDefault(x => x.Id == id);

            if (cliente is not null)
            {
                //Faz o de-para dos valores para atualizar a base de dados

                cliente.Nome = model.Nome;
                cliente.Idade = model.Idade;

                //Adicionando os pedidos
                if (listaPedidos is not null)
                {
                    //Limpando todos os registros e salvalndo apenas os novos registros
                    if (cliente.Pedidos is not null)  cliente.Pedidos.Clear();

                    foreach (var item in listaPedidos)
                    {
                        var pedido = _contexto.Pedido.Find(item);

                        if (pedido is not null)
                        {
                            if (cliente.Pedidos is null)
                                cliente.Pedidos = new List<Pedido> { pedido };
                            else
                                cliente.Pedidos.Add(pedido);
                        }

                    }
                }

                _contexto.Cliente.Update(cliente);
                _contexto.SaveChanges();

                return cliente;
            }

            return model;

        }
    }
}
