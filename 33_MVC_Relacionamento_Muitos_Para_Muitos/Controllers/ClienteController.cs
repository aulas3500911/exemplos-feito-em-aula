﻿using _33_MVC_Relacionamento_Muitos_Para_Muitos.Models;
using _33_MVC_Relacionamento_Muitos_Para_Muitos.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace _33_MVC_Relacionamento_Muitos_Para_Muitos.Controllers
{
    public class ClienteController : Controller
    {
        private readonly IPedidoRepository _pedidoRepository;
        private readonly IClienteRepository _clienteRepository;

        public ClienteController(IPedidoRepository PedidoRepository, IClienteRepository clienteRepository)
        {
            _pedidoRepository = PedidoRepository;
            _clienteRepository = clienteRepository;
        }

        public IActionResult Index()
        {
            var clientes = _clienteRepository
                .ListarTodos();

            return View(clientes);
        }

        public IActionResult Detail(int id)
        {
            var cliente = _clienteRepository
                .ObterUm(id);

            return View(cliente);
        }

        public IActionResult Create()
        {
            var listaPedidos = _pedidoRepository.ListarTodos();
            ViewBag.Pedidos = new MultiSelectList(listaPedidos, "Id", "Nome");
            return View();
        }

        [HttpPost]
        public IActionResult Create(Cliente model, int[]PedidosId)
        {
            if (ModelState.IsValid)
            {
                _clienteRepository
                    .SalvarCliente(model, PedidosId);

                return RedirectToAction("Index");
            }

            var listaPedidos = _pedidoRepository.ListarTodos();
            ViewBag.Pedidos = new MultiSelectList(listaPedidos, "Id", "Nome", PedidosId);  
            return View(model);
        }

        public IActionResult Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var cliente = _clienteRepository.ObterUm(id ?? 0);

            var listaPedidos = _pedidoRepository.ListarTodos();
            ViewBag.Pedidos = new MultiSelectList(listaPedidos, "Id", "Nome", cliente.Pedidos?.Select(x => x.Id).ToArray() ?? []);
            return View(cliente);
        }

        [HttpPost]
        public IActionResult Edit(int id, Cliente model, int[] PedidosId)
        {
            if (ModelState.IsValid)
            {
                model.Id = id;

                _clienteRepository.EditarCliente(id, model, PedidosId);

                return RedirectToAction("Index");
            }

            var listaPedidos = _pedidoRepository.ListarTodos();
            ViewBag.Pedidos = new MultiSelectList(listaPedidos, "Id", "Nome", model.Pedidos?.Select(x => x.Id).ToArray() ?? []);
            return View(model);
        }

    }
}
