﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using _33_MVC_Relacionamento_Muitos_Para_Muitos.Models.Comum;

namespace _33_MVC_Relacionamento_Muitos_Para_Muitos.Models
{
    // Define a classe como uma entidade mapeada para a tabela "tb_pedido"
    [Table("tb_pedido")]
    public class Pedido : IModel
    {
        // Define a propriedade "Id" como a chave primária da tabela
        [Key]
        public int Id { get; set; }
        [Required]
        [DisplayName("Nome do Pedido")]
        public string Nome { get; set; }
        [DisplayName("Valor do Pedido")]
        public decimal Valor { get; set; }

        [DisplayName("Selecione os Clientes")]
        public virtual ICollection<Cliente>? Clientes { get; set; }
    }
}
