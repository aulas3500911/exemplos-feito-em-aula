﻿using _33_MVC_Relacionamento_Muitos_Para_Muitos.Models.Comum;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace _33_MVC_Relacionamento_Muitos_Para_Muitos.Models
{
    [Table("tb_cliente")]
    public class Cliente : Pessoa, IModel
    {
        [Key]
        public int Id { get; set; }

        //Adicionando a propriedade Pedidos, como uma lista utilizando o ICollection, veja que foi incluido uma interrogação na variavel
        //para quando formos inserir um cliente, nao seja obrigatorio inserir um pedido
        [DisplayName("Selecione os Pedidos")]
        public virtual ICollection<Pedido>? Pedidos { get; set; }
    }
}
