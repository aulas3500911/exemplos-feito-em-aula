﻿namespace _25_MVC_TrabalhandoEntidades.Models
{
    //Criando a inteface IPessoa, para definir um contrato, onde qualquer classe que herdar esta interface,
    //deve implementar as propriedades que foram definidas
    public interface IPessoa
    {
        string Nome { get; set; }
        int Idade { get; set; }
    }
}
