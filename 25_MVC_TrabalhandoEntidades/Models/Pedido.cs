﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace _25_MVC_TrabalhandoEntidades.Models
{
    // Define a classe como uma entidade mapeada para a tabela "tb_pedido"
    [Table("tb_pedido")]
    public class Pedido
    {
        // Define a propriedade "Id" como a chave primária da tabela
        [Key]
        public int Id { get; set; }
        [Required]
        public string Nome { get; set; }
        public decimal Valor { get; set; }
    }
}
