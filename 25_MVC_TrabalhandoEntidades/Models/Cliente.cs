﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace _25_MVC_TrabalhandoEntidades.Models
{
    //Define a classe como uma entidade mapeada para a tabela "tb_cliente" utilizando o entity framework
    [Table("tb_cliente")]
    public class Cliente: IPessoa
    {
        // Define a propriedade "Id" como a chave primária da tabela
        [Key]
        public int Id { get; set; }

        // Define a propriedade "Nome" como obrigatória e com tamanho máximo de 100 caracteres
        [Required, MaxLength(100)]
        public string Nome { get; set; }

        public int Idade { get; set; }
    }
}
