﻿
//Uma forma de organizar os codigos em pacotes é utilizando as Namespaces. 
//Veja as classes e intefaces que estão na Solution Explorer na arvore do projeto.


//Iniciando uma name space para utilizar a classe oracle.
using BancoDados.Oracle;

//Iniciando uma name space para utilizar a classe Mysql.
using BancoDados.Mysql;


//Utlizando a classse Oracle do pacote "BancoDados.Oracle"
Oracle oracle = new Oracle();

//Utlizando a classse Mysql do pacote "BancoDados.Mysql"
Mysql mysql = new Mysql();  


//Outra forma de utilizar seria

//Oracle oracle2 = new BancoDados.Oracle.Oracle();

