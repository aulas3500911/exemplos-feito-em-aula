﻿
//Criando a namespace e organizando o codigo no pacote "BancoDados>Inteface"
namespace BancoDados.Interface;

public interface IBancoDados
{
    void ObterUmRegistro();
    void ObterMultiplosRegistro();
    void SalvarAtualziar();
}