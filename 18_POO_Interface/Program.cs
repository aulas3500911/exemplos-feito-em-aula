﻿
//Instanciando a classe Joao com os atibutos da interface
Joao joao = new Joao();

joao.Nome = "Joao";
joao.Documento = "123456";

joao.Curso = "Arquitetura";
joao.Disciplina = ".NET";

joao.Apresentar();

//Este metodo foi forçado a se implementado, pois esta utilizando a palavra "abstract" na classe pessoa
joao.MostrarDocumento();


//Criando uma classe abstrata de Pessoa
public abstract class Pessoa
{
    public string Nome { get; set; }
    public string Documento { get; set; }

    //Utilizando o palavra chave "abstract", forçamos a classe que esta herdando Pessoa a sobreescrever o metodo Apresentar.
    public abstract void Apresentar();


    //Utilizando a palavra chave "Virtual", podemos cria um metodo com comportamente, mas não é obrigatorio a sobreescrição nas outras classes 
    public virtual void MostrarDocumento()
    {
        Console.WriteLine($"Meu documento é: {Documento}");
    }
}


/*
 * UTILIZANDO HERANCA DAS CLASSES
 */

public class Aluno : Pessoa
{
    public string Curso { get; set; }


    //É obrigatorio a implementar o metodo Apresntar pois a classe pessoa obriga a fazer isso utilizando a palavra Abstract
    public override void Apresentar()
    {
        Console.WriteLine($"Eu sou um aluno e me chamo: {Nome}");
    }
}

public class Professor : Pessoa
{
    public string Disciplina { get; set; }

    public override void Apresentar()
    {
        Console.WriteLine($"Eu sou um professor e me chamo: {Nome}");
    }
}


//O .Net não permite Herança de multiplas classes, para isso é necessario o uso da interface
//Descomente o codigo para visualizar o erro que aparece

//public class Joao : Pessoa, Aluno, Professor
//{

//}






/*
 * UTILIZANDO HERANÇA DE INTEFACE AGORA
 */



//As interfaces são contratos que estabelecem um modelo que deve ser sempre implementado nas classes que herdarem ela.
//Alem de fornecer o suporte de herança MULTIPLAS.

//Veja o exemplo abaixo:
public interface IAluno
{
    string Curso { get; set; }
}

public interface IProfessor
{
    string Disciplina { get; set; }
}



//Classe Joao herdando pessoa, veja que o unico medodo que foi forçado a implementar da classe pessoa é o medodo "Apresentar"
//Já as propriedades da interface, todas devem ser implementadas.
public class Joao : Pessoa, IAluno, IProfessor
{
    public string Curso { get; set; }
    public string Disciplina { get; set; }

    public override void Apresentar()
    {
        Console.WriteLine($"Eu me chamo {Nome}");
    }
}