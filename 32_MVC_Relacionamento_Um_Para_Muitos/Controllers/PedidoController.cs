﻿using _32_MVC_Relacionamento_Um_Para_Muitos.Models;
using _32_MVC_Relacionamento_Um_Para_Muitos.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace _32_MVC_Relacionamento_Um_Para_Muitos.Controllers
{
    public class PedidoController : Controller
    {
        private readonly IPedidoRepository _pedidoRepository;
        private readonly IClienteRepository _clienteRepository;

        public PedidoController(IPedidoRepository PedidoRepository, IClienteRepository clienteRepository)
        {
            _pedidoRepository = PedidoRepository;
            _clienteRepository = clienteRepository;
        }

        public IActionResult Index()
        {
            var Pedidos = _pedidoRepository
                .ListarTodos();

            return View(Pedidos);
        }

        public IActionResult Detail(int id)
        {
            var Pedido = _pedidoRepository
                .ObterUm(id);

            return View(Pedido);
        }

        public IActionResult Create()
        {
            var listaClientes = _clienteRepository.ListarTodos();

            ViewBag.Clientes = new SelectList(listaClientes, "Id", "Nome");

            return View();
        }

        [HttpPost]
        public IActionResult Create(Pedido model)
        {
            if (ModelState.IsValid)
            {
                _pedidoRepository
                    .SalvarPedido(model);

                return RedirectToAction("Index");
            }

            var listaClientes = _clienteRepository.ListarTodos();
            ViewBag.Clientes = new SelectList(listaClientes, "Id", "Nome", model.ClienteId);
            return View(model);
        }

        public IActionResult Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var Pedido = _pedidoRepository.ObterUm(id ?? 0);

            var listaClientes = _clienteRepository.ListarTodos();
            ViewBag.Clientes = new SelectList(listaClientes, "Id", "Nome", Pedido.ClienteId);

            return View(Pedido);
        }

        [HttpPost]
        public IActionResult Edit(int id, Pedido model)
        {
            if (ModelState.IsValid)
            {
                model.Id = id;

                _pedidoRepository.EditarPedido(id, model);

                return RedirectToAction("Index");
            }

            var listaClientes = _clienteRepository.ListarTodos();
            ViewBag.Clientes = new SelectList(listaClientes, "Id", "Nome", model.ClienteId);
            return View(model);
        }
    }
}
