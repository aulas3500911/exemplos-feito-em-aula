﻿using System.ComponentModel.DataAnnotations;

namespace _32_MVC_Relacionamento_Um_Para_Muitos.Models.Comum
{
    public abstract class Pessoa
    {
        [Required, MaxLength(150)]
        public string Nome { get; set; }
        public int Idade { get; set; }
    }
}
