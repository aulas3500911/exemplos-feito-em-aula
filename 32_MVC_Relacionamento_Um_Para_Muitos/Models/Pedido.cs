﻿using _32_MVC_Relacionamento_Um_Para_Muitos.Models.Comum;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace _32_MVC_Relacionamento_Um_Para_Muitos.Models
{
    // Define a classe como uma entidade mapeada para a tabela "tb_pedido"
    [Table("tb_pedido")]
    public class Pedido : IModel
    {
        // Define a propriedade "Id" como a chave primária da tabela
        [Key]
        public int Id { get; set; }
        [Required]
        [DisplayName("Nome do Pedido")]
        public string Nome { get; set; }
        [DisplayName("Valor do Pedido")]
        public decimal Valor { get; set; }

        [DisplayName("Cliente")]
        public int ClienteId { get; set; }
        public virtual Cliente? Cliente { get; set; }
    }
}
