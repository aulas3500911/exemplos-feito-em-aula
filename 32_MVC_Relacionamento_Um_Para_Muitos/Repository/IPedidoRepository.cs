﻿using _32_MVC_Relacionamento_Um_Para_Muitos.Models;

namespace _32_MVC_Relacionamento_Um_Para_Muitos.Repository
{
    public interface IPedidoRepository
    {
        List<Pedido> ListarTodos();
        Pedido ObterUm(int id);
        Pedido SalvarPedido(Pedido model);
        Pedido EditarPedido(int id, Pedido model);
    }
}