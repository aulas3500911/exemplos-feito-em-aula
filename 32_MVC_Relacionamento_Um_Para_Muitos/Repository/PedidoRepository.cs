﻿using _32_MVC_Relacionamento_Um_Para_Muitos.Models;
using Microsoft.EntityFrameworkCore;

namespace _32_MVC_Relacionamento_Um_Para_Muitos.Repository
{
    public class PedidoRepository : IPedidoRepository
    {
        private readonly Contexto _contexto;

        public PedidoRepository(Contexto contexto)
        {
            _contexto = contexto;
        }

        public List<Pedido> ListarTodos()
        {
            var listaPedido = _contexto
                .Pedido
                .Include(x => x.Cliente) //Carregando todos os dados de cliente para exibir na listagem
                .ToList()
                .OrderBy(x => x.Id)
                .ToList();

            return listaPedido;
        }

        public Pedido ObterUm(int id)
        {
            var Pedido = _contexto
                .Pedido
                .Include(x => x.Cliente)
                .FirstOrDefault(x => x.Id == id);

            if (Pedido is null)
                return new Pedido();

            return Pedido;
        }

        public Pedido SalvarPedido(Pedido model)
        {
            _contexto.Pedido.Add(model);
            _contexto.SaveChanges();

            return model;
        }
        public Pedido EditarPedido(int id, Pedido model)
        {
            var Pedido = _contexto
                .Pedido
                .Include(x => x.Cliente)
                .FirstOrDefault(x => x.Id == id);

            if (Pedido is not null)
            {
                //Faz o de-para dos valores para atualizar a base de dados

                Pedido.Nome = model.Nome;
                Pedido.Valor = model.Valor;
                Pedido.ClienteId = model.ClienteId;

                _contexto.Pedido.Update(Pedido);
                _contexto.SaveChanges();

                return Pedido;
            }

            return model;

        }
    }
}
