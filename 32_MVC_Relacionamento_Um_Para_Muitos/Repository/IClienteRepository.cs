﻿using _32_MVC_Relacionamento_Um_Para_Muitos.Models;

namespace _32_MVC_Relacionamento_Um_Para_Muitos.Repository
{
    public interface IClienteRepository
    {
        List<Cliente> ListarTodos();
        Cliente ObterUm(int id);
        Cliente SalvarCliente(Cliente model);
        Cliente EditarCliente(int id, Cliente model);
    }
}