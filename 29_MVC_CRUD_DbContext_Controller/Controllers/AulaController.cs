﻿using _29_MVC_CRUD_DbContext_Controller.Models;
using Microsoft.AspNetCore.Mvc;

namespace _29_MVC_CRUD_DbContext_Controller.Controllers
{
    public class AulaController : Controller
    {
        private readonly Contexto _contexto;

        public AulaController(Contexto contexto)
        {
            _contexto = contexto;
        }

        public IActionResult ListarTodos()
        {
            //Select * from tb_cliente

            var clientes = _contexto.Cliente.ToList();

            return Json(clientes);
        }

        public IActionResult ListarUm()
        {
            //Select * from tb_cliente where id = 1
            //Select * from tb_cliente where id > 20

            //var cliente = _contexto.Cliente.FirstOrDefault(x => x.Idade > 20);
            //var cliente = _contexto.Cliente.Where(x => x.Idade > 20).FirstOrDefault();
            var cliente = _contexto.Cliente.Find(2);

            return Json(cliente);
        }

        public IActionResult Inserir()
        {
            var model = new Cliente
            {
                Nome = "Pedro",
                Idade = 15

            };

            //Insert into tb_cliente (Nome, Idade) values ('Pedro', 15)

            _contexto.Cliente.Add(model);
            _contexto.SaveChanges();


            return Json(new { });
        }


        public IActionResult Alterar()
        {
            var cliente = _contexto.Cliente.Find(4);

            cliente.Idade = 19;

            //Update tb_cliente set Idade = 19 where id = 4

            _contexto.Cliente.Update(cliente);
            _contexto.SaveChanges();


            return Json(cliente);
        }

        public IActionResult Deletar()
        {
            var cliente = _contexto.Cliente.Find(4);

            //Delete From tb_cliente where id = 4

            _contexto.Cliente.Remove(cliente);
            _contexto.SaveChanges();


            return Json(cliente);
        }
    }
}
